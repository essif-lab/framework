---
id: dependent
title: "Dependent"
scopeid: essifLab
type: concept
typeid: dependent
stage: draft
hoverText: "Dependent (in a Guardianship Arrangement): an Entity for the caring for and/or protecting/guarding/defending of which a Guardianship Arrangement has been established."
glossaryText: "an %%entity^entity%% for the caring for and/or protecting/guarding/defending of which a %%guardianship arrangement^guardianship-arrangement%% has been established."
date: 20210601
---

:::info Editor's Note
TNO to revise all below texts.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/dependent).
:::

### Short Description

The %%Guardianship pattern|pattern-guardianship%% provides an overview of how this concept fits in with related concepts.

### Purpose

### Criteria
