---
id: validator
title: "Validator"
scopeid: essifLab
type: concept
typeid: validator
stage: draft
hoverText: "Validator (functional component): the capability to determine whether or not (verified) data is valid to be used for some specific purpose(s)."
glossaryText: "the capability to determine whether or not (%%verified^verify%%) data is valid to be used for some specific purpose(s)."
date: 20210802
---

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/validator).
:::

### Short Description
A **validator** is is an (architectural) function (a functional component in the [eSSIF-Lab functional architecture](../essifLab-fw-func-arch)) that supports the %%Transaction Data Collector|transaction-data-collector%% as it tries to determine whether or not data obtained by the %%verifier|verifier%% comes with the assurances that are needed to be used for specific purpose(s).

:::info Editor's note
TNO (or others) to provide additional content of this file.
:::

### Purpose
The purpose of the validator function is.

### Criteria
A **validator** is a component in the [eSSIF-Lab functional architecture](../essifLab-fw-func-arch) whose function is to ... (tbd).

### Functionality
