---
id: credential-catalogue
title: "Credential Catalogue"
scopeid: essifLab
type: concept
typeid: credential-catalogue
stage: draft
hoverText: "Credential Catalogue: a functional component that has the capability to register and advertise the information about Credential Types that their respective Governing Parties have decided to disclose so as to enable other Parties to decide whether or not it is beneficial for them to use Credentials of such types."
glossaryText: "a functional component that has the capability to register and advertise the information about %%credential types^credential-type%% that their respective %%governing parties^governance%% have decided to disclose so as to enable other %%parties^party%% to decide whether or not it is beneficial for them to use %%credentials^credential%% of such types."
date: 20210601
---

:::info Editor's note
TNO to provide further content
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/credential-catalogue).
:::

### Short Description

A *Credential Catalogue* is a functional component that has the capability to register and advertise the information about Credential Types that their respective %%Governing Parties|governance%% have decided to disclose so as to enable other Parties to decide whether or not it is beneficial for them to use Credentials of such types.