---
id: documentation-interop
title: "Documentation Interoperability"
scopeid: essifLab
type: concept
typeid: documentation-interop
stage: draft
hoverText: "Documentation Interoperability: the property that a documentation system of making its content comprehensible for a variety of people that come from different backgrounds."
glossaryText: "the property that a documentation system of making its content comprehensible for a variety of people that come from different backgrounds."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/documentation-interop).
:::

### Related Concepts
