---
id: employee
title: "Employee"
scopeid: essifLab
type: concept
typeid: employee
stage: draft
hoverText: "Employee (of a Party): an Actor for whom/which it is realistic that it might execute Actions on behalf of that Party (called the Employer of that Actor)."
glossaryText: "an %%actor^actor%% for whom/which it is realistic that it might execute %%actions^action%% on behalf of a %%party^party%% (called the %%employer^employer%% of that %%actor^actor%%)."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/employee).
:::

### Short Description

The %%Parties, Actors and Actions pattern|pattern-party-actor-action%% provides an overview of how this concept fits in with related concepts.

### Purpose

### Criteria
