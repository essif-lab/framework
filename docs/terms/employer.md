---
id: employer
title: "Employer"
scopeid: essifLab
type: concept
typeid: employer
stage: draft
hoverText: "Employer (of an Actor): a Party on whose behalf this Actor (called an Employee of that Party) might execute Actions."
glossaryText: "a %%party^party%% on whose behalf an %%actor^actor%% (called an %%employee^employee%% of that %%party^party%%) might execute %%actions^action%%."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/employer).
:::

### Short Description

The %%Parties, Actors and Actions pattern|pattern-party-actor-action%% provides an overview of how this concept fits in with related concepts.

### Purpose

### Criteria
