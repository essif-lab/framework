---
id: guardian
title: "Guardian"
scopeid: essifLab
type: concept
typeid: guardian
stage: draft
hoverText: "Guardian: a Party that has been assigned rights and duties in a Guardianship Arrangement for the purpose of caring for and/or protecting/guarding/defending the Entity that is the Dependent in that Guardianship Arrangement."
glossaryText: "a %%party^party%% that has been assigned rights and duties in a %%Guardianship Arrangement^guardianship-arrangement%% for the purpose of caring for and/or protecting/guarding/defending the %%entity^entity%% that is the %%dependent^dependent%% in that Guardianship Arrangement."
date: 20210601
---

:::info Editor's Note
TNO to revise all below texts.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/guardian).
:::

### Short Description

The %%Guardianship pattern|pattern-guardianship%% provides an overview of how this concept fits in with related concepts.

### Purpose

### Criteria
