---
id: ecosystem
title: "Ecosystem"
scopeid: essifLab
type: concept
typeid: ecosystem
stage: draft
hoverText: "Ecosystem: a set of at least two (autonomous) Parties (its 'members') whose individual work complements that of other members, and is of benefit to the set as a whole."
glossaryText: "a set of at least two (autonomous) %%parties^party%% (its 'members') whose individual work complements that of other members, and is of benefit to the set as a whole."
date: 20210601
---

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/ecosystem).
:::

### Short Description
A Ecosystem is a set of at least two (autonomous) %%parties|party%% (the members of the ecosystem) whose individual work complements that of other members, and is of benefit to the set as a whole.

An ecosystem is distinct from a %%community|community%% in the sense that it is not (necessarily) an %%organization|organization%% that (actively) facilitates the cooperation between its members. A %%community|community%% is considered a specialization of the more generic 'ecosystem' concept.

### Purpose
The purpose of forming, or being part of an ecosystem, is that a %%party|party%% can focus on a specific topic area, rely on others to provide for its needs, and being appreciated for the value of its own work as it provides for the needs of others.

### Criterion
A Ecosystem is a set of at least two (autonomous) %%parties|party%% whose individual work complements that of others, and is of benefit to the set as a whole.
