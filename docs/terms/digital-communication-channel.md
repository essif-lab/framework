---
id: digital-communication-channel
title: "Digital Communication Channel"
scopeid: essifLab
type: concept
typeid: digital-communication-channel
stage: draft
hoverText: "Digital Communication Channel: a digital means by which two Digital Actors can exchange messages with one another."
glossaryText: "a digital means by which two %%digital actors^digital-actor%% can exchange messages with one another."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

See: %%Communication Channel|communication-channel%%.

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/digital-communication-channel).
:::

### Related Concepts
- %%Communication Session|communication-session%%