---
id: community
title: "Community"
scopeid: essifLab
type: concept
typeid: community
stage: draft
hoverText: "Community: an Organization that seeks to facilitate the cooperation between at least two Parties (its 'members') based on interests that these Parties share as each of them seeks to realize its own, individual Objectives."
glossaryText: "an %%organization^organization%% that seeks to facilitate the cooperation between at least two %%parties^party%%  (its 'members') based on interests that these %%parties^party%% share as each of them seeks to realize its own, individual %%objectives^objective%%"
date: 20210601
---

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/community).
:::

### Short Description
A Community is an %%organization|organization%% that seeks to facilitate the cooperation between at least two %%parties|party%% (referred to as its 'members') based on interests that these %%parties|party%% share as each of them seeks to realize its own, individual %%objectives|objective%%. Facilitating such a cooperation can be expressed in terms of %%objectives|objective%% that each of its members contributes to realizing.

Note that a single set of %%parties|party%% can seek different cooperations with one another, with different associated sets of %%objectives|objective%%. They are then different %%communities|community%% (even though they have the same members), because it is conceivable that another %%party|party%% will join one, but not both of them, or one member will leave one, but not both %%communities|community%%.

A community is a specialization of the more generic %%ecosystem|ecosystem%% in the sense that it is an %%organization|organization%% (which an ecosystem need not be) that (actively) facilitates the cooperation between its members, whereas in non-community ecosystems, this cooperation is not actively organized.

### Purpose
The ability to distinguish between %%communities|community%% and other %%organizations|organization%%, which simply seek to realize their own %%objectives|objective%% without any (explicit) regard to the objectives of other %%parties|party%% is particularly relevant in decentralized contexts. The characteristic of centralized contexts seems to be that an %%organization|organization%% that seeks to facilitate cooperation between other parties has (and uses) an ability to force these parties into complying with rules, working-instructions, etc., which may or may not be aligned with the interests of each of these parties. Such organizations are usually referred to as 'authorities'.

In decentralized contexts, the assumption is that all %%parties|party%% are autonomous (self-sovereign), which means that whatever such parties do (not) do is outside the scope of control of an %%organization|organization%% that seeks to facilitate cooperation between such parties. Such organizations will have objectives the realization of which do take the objectives and interests of other %%parties|party%% into regard.

### Criterion
A Community is an %%organization|organization%% that has one or more %%objectives|objective%% that seeks to facilitate the cooperation between at least two %%parties|party%% based on interests that these %%parties|party%% share as each of them seeks to realize its own, individual %%objectives|objective%%.
