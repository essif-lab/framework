---
id: issuer-policy
title: "Isuer Policy"
scopeid: essifLab
type: concept
typeid: issuer-policy
stage: draft
hoverText: "Issuer Policy: a Digital Policy that enables an operational Issuer component to function in accordance with the Objectives of its Principal."
glossaryText: "a %%digital policy^digital-policy%% that enables an operational %%issuer^issuer%% component to function in accordance with the %%objectives^objective%% of its %%principal^principal%%."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/issuer-policy).
:::

### Related Concepts
- %%Digital Policy|digital-policy%%
- %%Issuer|issuer%%
