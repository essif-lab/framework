---
id: transaction-request
title: "Transaction Request"
scopeid: essifLab
type: concept
typeid: transaction-request
stage: draft
hoverText: "Transaction Request: a message, send by a requesting Party to a providing Party, that initiates the negotiation of a new Transaction Agreement between these Parties for the provisioning of a specific product or service."
glossaryText: "a message, send by a requesting %%party^party%% to a providing %%party^party%%, that initiates the negotiation of a new %%transaction agreement^transaction-agreement%% between these %%parties^party%% for the provisioning of a specific product or service."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/transaction-request).
:::

### Related Concepts
- %%Transaction Agreement|transaction-agreement%%
- %%Transaction Form|transaction-form%%