---
id: transaction-id
title: "Transaction Id"
scopeid: essifLab
type: concept
typeid: transaction-id
stage: draft
hoverText: "Transaction Id (for a specific Business Transaction and a Participant): character string that this Participant uses to identify, and refer to, that Business Transaction."
glossaryText: "character string that this %%participant^participant%% uses to identify, and refer to, that %%business transaction^transaction%%."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

Explain that different %%transaction|transaction%% %%participants|participant%% are each likely to use their own %%identifiers|identifier%% for identifying, and referring to, the various %%transactions|transaction%% that they participate in. A %%participant|participant%% should communicate its %%transaction id|transaction-id%% to another %%participant|participant%% if it expects that other %%participant|participant%% to refer to the %%transaction|transaction%% in a way that it can dereference (i.e.: can use to identify the %%transaction|transaction%% with.

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/transaction-id).
:::

### Related Concepts
- %%Identifier|identifier%%
