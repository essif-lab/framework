---
id: transaction-proposal
title: "Transaction Proposal"
scopeid: essifLab
type: concept
typeid: transaction-proposal
stage: draft
hoverText: "Transaction (Agreement) Proposal: a Transaction Agreement that is 'in-the-making' (ranging from an empty document to a document that would be a Transaction Agreement if it were signed by all Participants)."
glossaryText: "a %%transaction agreement^transaction-agreement%% that is 'in-the-making' (ranging from an empty document to a document that would be a %%transaction agreement^transaction-agreement%% if it were signed by all %%participants^participant%%)."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/transaction-proposal).
:::

### Related Concepts
- %%Transaction Agreement|transaction-agreement%%
- %%Transaction Form|transaction-form%%