---
id: terminology
title: "Terminology"
scopeid: essifLabTerminology
type: concept
typeid: terminology
stage: draft
hoverText: "Terminology (in a Scope): a set of Terms that all have a Definition within some Scope, and are used for reasoning and communicating about one or more specific topics."
glossaryText: "a set of %%terms^term%% that all have a %%definition^definition%% within some %%scope(s)^scope%%, and are used for reasoning and communicating one or more specific topics."
date: 20210601
---

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/terminology).
:::

### Short Description
A **terminology** is a set of words and phrases (%%terms|term%%) that are used within some %%scope|scope%% for reasoning and communicating about specific topics. To limit the probability of misunderstandings between parties as they reason or communicate with each other, each of these terms is associated with a %%definition|definition%% within that scope.

A terminology may be viewed as a topic-specific subset of the scope's %%vocabulary|vocabulary%%.

A terminology may conveniently be documented in a %%glossary|glossary%%.

### Purpose
In order for %%parties|party%% to properly reason and/or communicate ideas (%%concepts|concept%%) about some topic - in particular when it is a specialist topic - they have to establish a set of terms, the meaning of which should be %%defined|definition%% in such a way that the likelihood of misunderstandings between them is minimized: a `**terminology**`

### Criteria
A **terminology** is a set of %%terms|term%% that are used within some %%scope|scope%% by %%parties|party%% to reason and communicate about some specific topic(s) for the purpose of reducing the likelihood of misunderstandings.

### Examples
- The set of words listed in the [eSSIF-Lab glossary](../essifLab-glossary) are used within the eSSIF-Lab community for reasoning and communicating about topics that serve the [eSSIF-Lab Objectives](../essifLab-objectives), which is part of the `eSSIF-Lab terminology`.
- The set of words listed in the [Sovrin glossary](https://sovrin.org/library/glossary/) are used within the Sovrin Governance Framework and its Controlled Documents to understand the meaning of the terms that the authors intended to convey. Thus they comprise the `Sovrin terminology`.