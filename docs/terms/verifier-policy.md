---
id: verifier-policy
title: "Verifier Policy"
scopeid: essifLab
type: concept
typeid: verifier-policy
stage: draft
hoverText: "Verifier Policy: a Digital Policy that enables an operational Verifier component to function in accordance with the Objectives of its Principal."
glossaryText: "a %%digital policy^digital-policy%% that enables an operational %%verifier^verifier%% component to function in accordance with the %%objectives^objective%% of its %%principal^principal%%."
date: 20210601
---

:::info Editor's note
TNO (or others) to provide the content of this file.
:::

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/verifier-policy).
:::

### Related Concepts
- %%Digital Policy|digital-policy%%
- %%Verifier|verifier%%
