---
id: ssi
title: "SSI (Self-Sovereign Identity)"
scopeid: eSSIFLab
type: term
typeid: ssi
stage: draft
hoverText: "SSI (Self-Sovereign Identity): a term that has many different interpretations, and that we use to refer to concepts/ideas, architectures, processes and technologies that aim to support (autonomous) Parties as they negotiate and execute electronic Transactions with one another."
glossaryText: "SSI (Self-Sovereign Identity) is a term that has many different interpretations, and that we use to refer to concepts/ideas, architectures, processes and technologies that aim to support (autonomous) %%parties^party%% as they negotiate and execute electronic %%transactions^transaction%% with one another."
date: 20210601
---

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/ssi).
:::

### Short Description
SSI is an abbreviation of %%Self-Sovereign Identity|self-sovereign-identity%%