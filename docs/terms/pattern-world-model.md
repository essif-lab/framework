---
id: pattern-world-model
title: "World Model"
scopeid: essifLab
type: pattern
typeid: world-model
stage: draft
hoverText: "The eSSF-Lab World Model is the set of Concepts, relations between them (Patterns), and principles (that are the starting point for eSSIF-Lab's thinking)."
glossaryText: "The set of %%concepts^concept%%, relations between them (%%patterns^pattern%%), and [Principles](essifLab-principles) (that are the starting point for eSSIF-Lab's thinking)."
date: 20210601
---

import useBaseUrl from '@docusaurus/useBaseUrl'

:::info Editor's note
This is work that is being envisaged.
:::

The **eSSIF-Lab World Model** consists of
- the set of %%concepts|concept%%, as listed in the [Glossary](../essifLab-Glossary),
- relations between these concepts, as explained in the various concept-descriptions, and in the various %%patterns|pattern%%, as listed in the [Patterns List](../essifLab-pattern-list).
- a set of [Principles](../essifLab-principles) that we take as starting points for our thinking.

## eSSIF-Lab World Model

This document summarizes the most fundamental concepts of the world model and their properties, and points to the patterns that provide more details.

:::caution
This page has been moved to the [eSSIF-Lab Framework on Github](https://essif-lab.github.io/framework/docs/terms/pattern-world-model).
:::

### The Universe consists of Entities

eSSIF-Lab sees the world (universe) as being filled with %%entities|entity%%, i.e. things (people and animals included) that exist. Our world view is shaped by the distinctions that eSSIF-Lab makes as it classifies such entities into well-defined categories, each of which having specific characteristics.

### Parties: Entities that manage and use a Knowledge autonomously

One of the most fundamental categories is called %%party|party%%. A party (typical examples of which are %%organizations|organization%% (governments, enterprises) and individual human beings) is defined as an %%entity|entity%% that manages and uses its own %%knowledge|knowledge%% autonomously. It is its own sovereign, i.e. within its %%scope of control|scope-of-control%%, everything is guided by its own, subjective %%knowledge|knowledge%%. This includes all decision making, execution of %%actions|action%%, %%risks|risk%%, %%governance|governance%%, etc., etc. We also postulate that every party has %%objectives|objective%% that it pursues. A large part of its knowledge revolves about managing these objectives (which includes %%governance|governance%% and risk management) and making sure they get realized.

While parties are their own (self)sovereigns and in principle can do as they please, they live in a universe with many other parties, that are also 'self sovereigns'. This means that they need to come to terms with one another as they have interactions with one another that may influence the %%knowledge|knowledge%% of the participating parties, and have other consequences as well. Such %%transactions|transaction%% between parties is not only one of the focal points of this framework, but the reason we have created it.

You can learn more about parties from the %%parties, actors and action pattern|pattern-party-actor-action%%.


### Actors: Entities that can act (do things)

Another fundamental category is called %%actor|actor%%, which is defined as an %%entity|entity%% that can act (do things). It is important to note that some actors are parties (e.g. human beings) while others are not (e.g. robots, or organizations). As an actor does something (executing an %%action|action%%), it uses the %%knowledge|knowledge%% of one specific %%party|party%% as the main guidance for executing that action (it may use knowledge of other parties as well). We say that the actor executes that action on behalf of that party. Actors may execute different actions on behalf of different parties. It is a particular contribution of this framework that using it allows one to determine which actor executes what action on behalf of what party. This is particularly relevant in the context of two interacting/transacting parties.

You can learn more about actors from the %%parties, actors and action pattern|pattern-party-actor-action%%.

### Jurisdiction: Authority with mechanisms for defining rules, enforcing them, and resolving disputes

A third fundamental category is called %%jurisdiction|jurisdiction%%, which is a foundational concept for organizing collaborations between %%parties|party%%, e.g. in %%communities|community%% or %%ecosystems|ecosystem%%. Basically, a jurisdiction is an %%authority|authority%% that has mechanisms for for defining and maintaining rules, enforcement thereof, and a mechanism for resolving conflicts within its %%scope of control|scope-of-control%%.

You can learn more about jurisdictions from the %%jurisdictions pattern|pattern-jurisdiction%%.